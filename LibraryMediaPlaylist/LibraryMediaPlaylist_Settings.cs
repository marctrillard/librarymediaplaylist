﻿using CsPlayer_Lib.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMediaPlaylist
{
    class LibraryMediaPlaylist_Settings
    {
        public readonly static String Media_LocalPath = BasicModulesServices.Instance.MediaLocalPath;

        public static String LibraryMediaPlaylist_LocalPath
        {
            get
            { return Path.Combine(BasicModulesServices.Instance.MediaLocalPath, @"Working\LibraryMediaPlaylist"); }
        }
    }
}

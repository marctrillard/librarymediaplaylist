﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryMediaPlaylist
{
    class Video
    {

        public string Idcmp { get; set; }
        public string FilePath { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }

        public Video(string Idcmp, string FilePath, string DateStart, string DateEnd)
        {
            this.Idcmp = Idcmp;
            this.FilePath = FilePath;
            this.DateStart = DateStart;
            this.DateEnd = DateEnd;
        }


        public Video(string Idcmp, string FilePath)
        {
            this.Idcmp = Idcmp;
            this.FilePath = FilePath;
        }
    }
}

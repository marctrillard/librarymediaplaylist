﻿using CsPlayer_Lib.Services;
using Finisar.SQLite;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace LibraryMediaPlaylist
{
    /// <summary>
    /// Logique d'interaction pour UserControl1.xaml
    /// </summary>
    public partial class LibraryMediaPlaylist : UserControl, INotifyPropertyChanged
    {
        #region Private Members
        private int _currentVideoIdx = 0;
        Video _currentVideo;
        string[] _videos;

        int _height = 0;
        int _width = 0;
        public new int Height
        { get { return _height; } set { if (!_height.Equals(value)) { _height = value; OnPropertyChanged("Height"); } } }
        public new int Width
        { get { return _width; } set { if (!_width.Equals(value)) { _width = value; OnPropertyChanged("Width"); } } }

        string dbFile = System.IO.Path.Combine(BasicModulesServices.Instance.MediaLocalPath, @"Working\LibraryMediaPlaylist\" + DB_FILENAME);
        string dbVersion = "3";
        string dbCompress = "True";

        string playlistFile = System.IO.Path.Combine(BasicModulesServices.Instance.MediaLocalPath, @"Working\LibraryMediaPlaylist\" + FILENAME);

        SQLiteDataReader sqlite_datareader;
        private Object lockDb = new Object();
        DispatcherTimer timerCopyDb = new DispatcherTimer();


        string path = LibraryMediaPlaylist_Settings.LibraryMediaPlaylist_LocalPath;
        #endregion

        #region Constantes
        private const string TAG_LOG = "ModulePlaylist ==> ";
        private const string FILENAME = "playlist-cmsplayer.txt";
        private const string DB_FILENAME = "playlist_logs.sqlite";
        private const string DB_FILENAME_COPY = "playlist_logs_copy.sqlite";
        private const string SOURCE_DIR = "sourceDirectory";
        // Default values
        private string DEFAULT_SOURCEDIR = LibraryMediaPlaylist_Settings.LibraryMediaPlaylist_LocalPath;
        #endregion

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Alls the properties changed.
        /// </summary>
        public virtual void AllPropertiesChanged()
        {
            OnPropertyChanged(null);
        }
        #endregion // INotifyPropertyChanged Members

        #region Constructor
        public LibraryMediaPlaylist(int[] zoneCoord, JToken miniSeq, JToken dynamicParams)
        {
            // Get dynamic parameters
            //JObject inner = dynamicParams.Value<JObject>();
            //List<string> dynamicKeys = inner.Properties().Select(p => p.Name).ToList();
            //_sourceDir = ((dynamicKeys.Contains(SOURCE_DIR)) && ((string)dynamicParams[SOURCE_DIR] != "")) ? (string)dynamicParams[SOURCE_DIR] : System.IO.Path.Combine(LibraryMediaPlaylist_Settings.Media_LocalPath, DEFAULT_SOURCEDIR);

            InitializeComponent();

            InitDb();
            PurgeLogs();

            // HERE on parse le fichier texte contenant les urls des médias
            _videos = ParsePlaylistFile();
            
            timerCopyDb.Interval = TimeSpan.FromMinutes(20);
            timerCopyDb.Start();
            timerCopyDb.Tick += (o, args) =>
            {
                timerCopyDb.Stop();
                CopyDbFile();
            };

            DispatcherTimer timerStartCopy = new DispatcherTimer();
            timerStartCopy.Interval = TimeSpan.FromMinutes(15);
            timerStartCopy.Start();
            timerStartCopy.Tick += (o, args) =>
            {
                timerStartCopy.Stop();
                CopyDbFile();
            };
        }
        #endregion

        #region UI Event
        // HERE est appelé automatiquement à la fin d'une vidéo ou manuellement lors de la diffusion d'une image
        private void mediaEltPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            UpdateLog(_currentVideo.Idcmp, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            LoadNextVideo();
        }
        
        private void mediaEltPlayer_Loaded(object sender, RoutedEventArgs e)
        {
            //mediaEltPlayer.Volume = (double)AppSettings.VolumeGeneral / 100.0; // Because (0 <= Volume <= 1) and (0 <= VolumeGeneral <= 100)
            LoadNextVideo();
        }

        private void mediaEltPlayer_Unloaded(object sender, RoutedEventArgs e)
        {
            mediaEltPlayer.Source = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        // HERE la principale fonction qui fait tout
        private void LoadNextVideo()
        {
            try
            {
                //parse playlistFile instead of getting videos from directory
                //string[] videos = ParsePlaylistFile();

                if (_videos.Length == 0)
                    return;

                if (_currentVideoIdx >= _videos.Length)
                    _currentVideoIdx = 0;

                // HERE on parse chaque ligne du fichier texte
                string[] currentVideoTab = _videos[_currentVideoIdx].Split(';');

                _currentVideo = new Video(currentVideoTab[0], currentVideoTab[1]);
                
                string MediaPath = _currentVideo.FilePath;
                Uri MediaUri = new Uri(MediaPath, UriKind.Absolute);

                // HERE ici c'est une image on l'affiche pendant la durée définie dans le fichier texte puis le timertick fait appel au mediaEnded
                if (_currentVideo.FilePath.EndsWith("png") || _currentVideo.FilePath.EndsWith("jpg"))
                {
                    //Logs_Services.Instance.Log(TAG_LOG + "start image display");

                    ImageElement.Source = new BitmapImage(MediaUri);
                    ImageElement.Visibility = Visibility.Visible;

                    DispatcherTimer timer = new DispatcherTimer();
                    int duration = int.Parse(currentVideoTab[2]);
                    timer.Interval = TimeSpan.FromSeconds(duration);
                    timer.Tick += (o, args) =>
                    {
                        //Logs_Services.Instance.Log(TAG_LOG + "stop image display");
                        timer.Stop();
                        ImageElement.Visibility = Visibility.Collapsed;
                        mediaEltPlayer_MediaEnded(null, null);
                    };
                    timer.Start();
                } else
                {
                    mediaEltPlayer.Source = new Uri(MediaPath);
                    mediaEltPlayer.Play();
                }

                InsertLog(_currentVideo.Idcmp, _currentVideo.FilePath, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                _currentVideoIdx++;
            }
            catch (Exception ex)
            {
                Logs_Services.Instance.Log(TAG_LOG + "LoadNextVideo exception : " + ex.StackTrace + "\n" + ex.Message);
            }
        }
        #endregion


        private string[] ParsePlaylistFile()
        {
            return System.IO.File.ReadAllLines(playlistFile);
        }

        private void CopyDbFile() {
            try
            {
                if (File.Exists(path + "\\" + DB_FILENAME))
                    System.IO.File.Copy(path + "\\" + DB_FILENAME, path + "\\" + DB_FILENAME_COPY, true);
                timerCopyDb.Start();
            }
            catch (Exception e) { }
        }

        #region Database stuffs
        public void InitDb()
        {
            bool isNew = false;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            if (!File.Exists(path + "\\" + DB_FILENAME))
            {
                isNew = true;
                Logs_Services.Instance.Log(TAG_LOG + "new DB created at");
                Logs_Services.Instance.Log(TAG_LOG + path + "\\" + DB_FILENAME);
            }

            if (isNew)
                CreateTable(isNew);
        }

        public void CreateTable(bool isNew)
        {
            try
            {
                string sqlite_conn = string.Format("Data Source={0};Version={1};New={2};Compress={3};", dbFile, dbVersion, isNew ? "True" : "False", dbCompress);
                lock (lockDb)
                {
                    using (SQLiteConnection connection = new SQLiteConnection(sqlite_conn))
                    {
                        connection.Open();
                        using (SQLiteCommand cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "CREATE TABLE logs (Id INTEGER PRIMARY KEY, idcmp TEXT, filepath TEXT, datedebut TEXT, datefin TEXT default \'\')";
                            cmd.ExecuteNonQuery();
                        }
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logs_Services.Instance.Log(TAG_LOG + "CreateTable exception : " + ex.StackTrace + "\n" + ex.Message);
            }
        }

        public void InsertLog(string idcmp, string filePath, string datedebut)
        {
            try
            {
                string sqlite_conn = string.Format("Data Source={0};Version={1};New={2};Compress={3};", dbFile, dbVersion, "False", dbCompress);
                lock (lockDb)
                {
                    using (SQLiteConnection connection = new SQLiteConnection(sqlite_conn))
                    {
                        connection.Open();
                        using (SQLiteCommand cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "INSERT INTO logs (idcmp, filepath, datedebut) VALUES ('" + idcmp + "', '" + filePath + "', '" + datedebut + "');";
                            cmd.ExecuteNonQuery();
                        }
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logs_Services.Instance.Log(TAG_LOG + "InsertLog exception : " + ex.StackTrace + "\n" + ex.Message);
            }
        }

        public void UpdateLog(string idcmp, string datefin)
        {
            try
            {
                string sqlite_conn = string.Format("Data Source={0};Version={1};New={2};Compress={3};", dbFile, dbVersion, "False", dbCompress);
                lock (lockDb)
                {
                    using (SQLiteConnection connection = new SQLiteConnection(sqlite_conn))
                    {
                        connection.Open();
                        using (SQLiteCommand cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "UPDATE logs set datefin='" + datefin + "' where Id in (Select Id From logs where idcmp like " + idcmp + " ORDER BY Id DESC LIMIT 1);";
                            cmd.ExecuteNonQuery();
                        }
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logs_Services.Instance.Log(TAG_LOG + "UpdateLog exception : " + ex.StackTrace + "\n" + ex.Message);
            }
        }

        public void GetLogs(string cardUid)
        {
            try
            {
                string sqlite_conn = string.Format("Data Source={0};Version={1};New={2};Compress={3};", dbFile, dbVersion, "False", dbCompress);
                lock (lockDb)
                {
                    using (SQLiteConnection connection = new SQLiteConnection(sqlite_conn))
                    {
                        connection.Open();
                        using (SQLiteCommand cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "SELECT * FROM logs;";
                            sqlite_datareader = cmd.ExecuteReader();
                            while (sqlite_datareader.Read())
                            {
                                string idcmp = (string)sqlite_datareader["idcmp"];
                                string dateDebut = (string)sqlite_datareader["datedebut"];
                                string filePath = (string)sqlite_datareader["filepath"];
                                string dateFin = (string)sqlite_datareader["datefin"];

                                Logs_Services.Instance.Log(TAG_LOG + "");
                            }
                        }
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logs_Services.Instance.Log(TAG_LOG + "GetLogs exception : " + ex.StackTrace + "\n" + ex.Message);
            }
        }

        public void PurgeLogs()
        {
            try
            {
                string sqlite_conn = string.Format("Data Source={0};Version={1};New={2};Compress={3};", dbFile, dbVersion, "False", dbCompress);
                lock (lockDb)
                {
                    using (SQLiteConnection connection = new SQLiteConnection(sqlite_conn))
                    {
                        connection.Open();
                        using (SQLiteCommand cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "DELETE FROM logs WHERE datedebut <= date('now','-7 day');";
                            cmd.ExecuteNonQuery();

                            //cmd.CommandText = "UPDATE tas set WinnerName = \""+_currentCity+"\" Where Id ="+_intCount+";";
                            //cmd.ExecuteNonQuery();
                        }
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logs_Services.Instance.Log(TAG_LOG + "PurgeLogs exception : " + ex.StackTrace + "\n" + ex.Message);
            }
        }
        #endregion
    }
}
